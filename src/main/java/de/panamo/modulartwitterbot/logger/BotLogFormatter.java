package de.panamo.modulartwitterbot.logger;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class BotLogFormatter extends Formatter {

    private DateFormat format = new SimpleDateFormat("HH:mm:ss");

    public String format(LogRecord record) {
        StringBuilder trace = new StringBuilder();
        if (record.getThrown() != null) {
            StringWriter writer = new StringWriter();
            record.getThrown().printStackTrace(new PrintWriter(writer));
            trace.append(writer).append("\n");
        }
        return "[" + this.format.format(record.getMillis()) + "/"
                + record.getLevel().getLocalizedName() + "] " + super.formatMessage(record)
                + "\n" + trace.toString();
    }
}
