package de.panamo.modulartwitterbot.console;


import jline.console.ConsoleReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Console implements Runnable {
    private List<ConsoleHandler> handlers = new ArrayList<>();
    private ConsoleReader consoleReader;

    public Console() {
        try {
            this.consoleReader = new ConsoleReader(System.in, System.out);
            this.consoleReader.setExpandEvents(false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            String line;
            while ((line = this.consoleReader.readLine()) != null)
                for(ConsoleHandler handler : this.handlers)
                    handler.handleInput(line);
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public void println(CharSequence charSequence) {
        try {
            this.consoleReader.println(charSequence);
            this.consoleReader.drawLine();
            this.consoleReader.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void println() {
        try {
            this.consoleReader.println();
            this.consoleReader.drawLine();
            this.consoleReader.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addHandler(ConsoleHandler handler) {
        this.handlers.add(handler);
    }

    public void printStartScreen() {
        this.clearScreen();
        this.println("___________       .__  __    __              __________        __   " + "\n" +
                "\\__    ___/_  _  _|__|/  |__/  |_  __________\\______   \\ _____/  |_ " + "\n" +
                "  |    |  \\ \\/ \\/ /  \\   __\\   __\\/ __ \\_  __ \\    |  _//  _ \\   __\\" + "\n" +
                "  |    |   \\     /|  ||  |  |  | \\  ___/|  | \\/    |   (  <_> )  |  " + "\n" +
                "  |____|    \\/\\_/ |__||__|  |__|  \\___  >__|  |______  /\\____/|__|  " + "\n" +
                "                                      \\/             \\/             " + "\n" +
                "                                                         by Panamo");
        this.println();
    }

    public void clearScreen() {
        try {
            this.consoleReader.clearScreen();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
