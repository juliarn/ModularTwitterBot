package de.panamo.modulartwitterbot;

import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;
import java.io.*;
import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

public class Configuration {
    private static Yaml yaml;
    private Map<String, Object> data;
    private File file;

    static {
        DumperOptions options = new DumperOptions();
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        yaml = new Yaml(options);
    }

    public Configuration(File file) {
        if(!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.file = file;
    }

    public Configuration(Configuration defaultData) {
        this(defaultData.file);
        this.data = defaultData.data;
    }

    public Configuration(File file, Configuration defaultData) {
        this(file);
        this.data = defaultData.data;
    }

    public Configuration(File file, Map<String, Object> defaultData) {
        this(file);
        this.data = defaultData;
    }

    public void set(String key, Object value) {
        this.data.put(key, value);
    }

    public Object get(String key) {
        return this.data.get(key);
    }

    public String getString(String key) {
        return String.valueOf(this.get(key));
    }

    public int getInt(String key) {
        return (Integer) this.get(key);
    }

    public long getLong(String key) {
        return (Long) this.get(key);
    }

    public double getDouble(String key) {
        return (Double) this.get(key);
    }

    public float getFloat(String key) {
        return (Float) this.get(key);
    }

    public List<?> getList(String key) {
        return (List<?>) this.get(key);
    }

    public List<String> getStringList(String key) {
        return this.getList(key).stream().map(String::valueOf).collect(Collectors.toList());
    }

    @SuppressWarnings("unchecked")
    public <T> T getDefault(String key, T defaultValue) {
        if(this.data.containsKey(key))
            return (T) this.get(key);
        this.set(key, defaultValue);
        return defaultValue;
    }

    @SuppressWarnings("unchecked")
    public void load() throws FileNotFoundException {
        this.data = yaml.loadAs(new FileInputStream(this.file), HashMap.class);
        if(this.data == null)
            this.data = new HashMap<>();
    }

    public void save() throws IOException {
        yaml.dump(this.data, new FileWriter(this.file));
    }

    public <T> T dataToObject(Class<T> clazz) throws IllegalAccessException, InstantiationException, NoSuchFieldException {
        T object = clazz.newInstance();
        for(Map.Entry<String, Object> entry : this.data.entrySet()) {
            Field field = clazz.getDeclaredField(entry.getKey());
            field.setAccessible(true);
            field.set(object, entry.getValue());
        }
        return object;
    }

    public Set<String> getDataKeys() {
        return this.data.keySet();
    }

    public File getFile() {
        return file;
    }
}
