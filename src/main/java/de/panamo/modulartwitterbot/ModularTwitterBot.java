package de.panamo.modulartwitterbot;

import de.panamo.modulartwitterbot.command.Command;
import de.panamo.modulartwitterbot.command.CommandManager;
import de.panamo.modulartwitterbot.console.Console;
import de.panamo.modulartwitterbot.logger.BotLogger;
import de.panamo.modulartwitterbot.module.ModuleManager;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.conf.ConfigurationBuilder;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ModularTwitterBot {
    private static ModularTwitterBot instance;
    private Console console = new Console();
    private BotLogger botLogger;
    private Configuration botConfiguration;
    private Twitter twitter;
    private ModuleManager moduleManager;
    private CommandManager commandManager = new CommandManager();

    private ModularTwitterBot() {
        instance = this;
        Runtime.getRuntime().addShutdownHook(new Thread(this::shutdown));

        this.console.printStartScreen();
        this.console.addHandler(this.commandManager);

        this.botLogger = new BotLogger(this.console);
        this.botLogger.info("Loading config ...");

        this.botConfiguration = new Configuration(new File("config.yml"));
        try {
            this.botConfiguration.load();
        } catch (FileNotFoundException exception) {
            this.botLogger.logError(exception, "Error while loading the configuration");
            System.exit(1);
        }

        String key = this.botConfiguration.getDefault("consumerKey", "KEY");
        String secret = this.botConfiguration.getDefault("consumerSecret", "SECRET");
        String token = this.botConfiguration.getDefault("accessToken", "TOKEN");
        String tokenSecret = this.botConfiguration.getDefault("accessTokenSecret", "TOKENSECRET");

        this.commandManager.registerCommand(null, new Command("shutdown", "Stops the bot.", "stop") {
            @Override
            public void execute(String[] args) {
                System.exit(0);
            }
        });
        this.commandManager.registerCommand(null, new Command("help", "Shows help.") {
            @Override
            public void execute(String[] args) {
                for(Command command : commandManager.getCommands())
                    botLogger.info(command.getName() + " - " + command.getDescription());
            }
        });

        this.botLogger.info("Loading Modules ...");
        this.moduleManager = new ModuleManager(this, new File("modules"));
        this.moduleManager.loadModules();

        this.botLogger.info("Connecting to the Twitter-Application ...");

        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
        configurationBuilder.setDebugEnabled(true)
                .setOAuthConsumerKey(key)
                .setOAuthConsumerSecret(secret)
                .setOAuthAccessToken(token)
                .setOAuthAccessTokenSecret(tokenSecret);

        TwitterFactory twitterFactory = new TwitterFactory(configurationBuilder.build());
        this.twitter = twitterFactory.getInstance();

        try {
            User user = this.twitter.showUser(this.twitter.getScreenName());
            this.botLogger.info("Successfully connected to the user @"
                    + user.getScreenName() + " with the name " + user.getName());
        } catch (TwitterException exception) {
            this.botLogger.logError(exception, "Error while retrieving the userdata from Twitter");
            System.exit(1);
        }

        this.moduleManager.enableModules();
        this.console.run();
    }


    public void shutdown() {
        this.botLogger.info("Shutting down the bot ...");
        this.moduleManager.disableModules();
        this.botLogger.info("Saving the configuration ...");
        try {
            this.botConfiguration.save();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Console getConsole() {
        return console;
    }

    public BotLogger getBotLogger() {
        return botLogger;
    }

    public Configuration getBotConfiguration() {
        return botConfiguration;
    }

    public Twitter getTwitter() {
        return twitter;
    }

    public ModuleManager getModuleManager() {
        return moduleManager;
    }

    public CommandManager getCommandManager() {
        return commandManager;
    }


    public static ModularTwitterBot getInstance() {
        return instance;
    }

    public static void main(String[] args) {
        new ModularTwitterBot();
    }
}
