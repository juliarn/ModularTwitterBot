package de.panamo.modulartwitterbot.command;


import de.panamo.modulartwitterbot.module.Module;

public abstract class Command {
    private String name, description;
    private String[] aliases;
    private Module module;

    public Command(String name, String description, String... aliases) {
        this.name = name;
        this.description = description;
        this.aliases = aliases;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String[] getAliases() {
        return aliases;
    }

    public Module getModule() {
        return module;
    }

    public void setModule(Module module) {
        this.module = module;
    }

    public abstract void execute(String[] args);
}
